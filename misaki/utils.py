import argparse

from redis import Redis

from misaki.config import config


def progressbar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end="\r"):
    """
    Call in a loop to create terminal progress bar
    @:param int iteration: current iteration
    @:param int total: total iterations
    @:param str prefix: prefix string
    @:param str suffix: suffix string
    @:param int decimals: positive number of decimals in percent complete
    @:param int length: character length of bar (Int)
    @:param str fill: bar fill character
    @:param str printEnd: end character (e.g. "\r", "\r\n")
    """
    percent = ('{0:.' + str(decimals) + 'f}').format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r{} |{}| {}% {}'.format(prefix, bar, percent, suffix), end=print_end)
    if iteration == total:
        print()


def restricted_float(min: float, max: float) -> callable:
    """
    Enables a float parameter in argparse to be restricted
    :param float min: minimum value
    :param float max: maximum value
    :return callable
    """
    # we use this structure to allow the internal func to have args min and max in it's scope
    def _restricted_float(x) -> float:
        """
        Internal func which is called by argparse
        :param x
        :return float
        """
        try:
            x = float(x)
        except ValueError:
            raise argparse.ArgumentTypeError('{} not a floating-point literal'.format(x))
        if x < min or x > max:
            raise argparse.ArgumentTypeError('{} not in range [0.0, 1.0]'.format(x))
        return x
    return _restricted_float


def get_redis_client() -> Redis:
    """
    Returns a Redis instance
    :return Redis
    """
    return Redis(host=config.redis.host, port=config.redis.port, db=config.redis.db)
