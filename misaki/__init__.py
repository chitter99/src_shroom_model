"""
Provide easy access to the most important function of this module.
"""

from misaki.model import predict_from_base64, predict_from_path, predict, train, prepare_dataset as prepare
from misaki.cli import cli
