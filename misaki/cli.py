import argparse
from enum import Enum

from misaki.misaki import prepare, train, predict
from misaki.utils import restricted_float


class Command(Enum):
    """ Command type for the cli """
    Default = 0
    Prepare = 1
    Train = 2
    Predict = 3


def cli():
    """ Starts the cli pipeline """
    args = parser.parse_args()
    if args.cmd == Command.Prepare:
        prepare(args.input)
        pass
    elif args.cmd == Command.Train:
        train(args.test_size)
        pass
    elif args.cmd == Command.Predict:
        predict(args.input)
        pass
    else:
        parser.parse_args(['--help'])
    print('[INFO] Done!')


parser = argparse.ArgumentParser(description='Misaki cli')
parser.set_defaults(cmd=Command.Default)
subparsers = parser.add_subparsers(help='how may I serve you?')

# Prepare command
parser_prepare = subparsers.add_parser('prepare', help='prepare dataset for training')
parser_prepare.set_defaults(cmd=Command.Prepare)
parser_prepare.add_argument('input', help='dataset input folder, each subdirectory should be a class')

# Train command
parser_train = subparsers.add_parser('train', help='train model')
parser_train.set_defaults(cmd=Command.Train)
parser_train.add_argument('--test-size', help='data used for testing in %',
                          type=restricted_float(min=0.0, max=1.0), default=0.2)

# Predict command
parser_predict = subparsers.add_parser('predict', help='predict image please')
parser_predict.set_defaults(cmd=Command.Predict)
parser_predict.add_argument('input', help='input image')

if __name__ == '__main__':
    cli()
