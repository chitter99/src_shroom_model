from rq import Connection, Worker

from misaki.utils import get_redis_client
from misaki.config import config

# preloading modules that worker func is using later
import misaki.misaki
import misaki.model


def bootstrap():
    """
    Starts the rq worker
    """
    with Connection():
        worker = Worker(config.redis.queue, connection=get_redis_client())
        worker.work()
