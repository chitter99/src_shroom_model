import configparser
import os


class ConfigCollection:
    """ Holds a config dict and allows them to be accessed via attributes """
    def __init__(self, collection):
        """
        :param dict collection: config values
        """
        self._collection = collection

    def __getattr__(self, item):
        return self._collection[item]


class Path(ConfigCollection):
    pass


class Model(ConfigCollection):
    pass


class Redis(ConfigCollection):
    pass


def _translate_path(path) -> str:
    """
    Translates a relative path inside the config.ini to a absolute path
    :param str path: path in config.ini (e.g. volume/latest.h5)
    :return: str
    """
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', path))


class Config:
    """ Loads and sets up all config values for easy access """
    def __init__(self, filename='config.ini'):
        """
        :param str filename: config.ini location
        """
        self._config = configparser.ConfigParser()
        self._config.read(_translate_path(filename))

        self._path = dict()
        self._model = dict()
        self._redis = dict()

        self._load()

    def _load(self):
        """ loads all values from config and assigns them to the correct config collection """
        self._path['output_model'] = _translate_path(self._config.get('path', 'output_model'))
        self._path['output_matrix'] = _translate_path(self._config.get('path', 'output_matrix'))
        self._path['output_checkpoint_models'] = \
            _translate_path(self._config.get('path', 'output_checkpoint_models'))
        self._path['classes_lexicon'] = _translate_path(self._config.get('path', 'classes_lexicon'))
        self.path = Path(self._path)

        self._model['epochs'] = int(self._config.get('model', 'epochs'))
        self._model['batch_size'] = int(self._config.get('model', 'batch_size'))
        self._model['classes'] = int(self._config.get('model', 'classes'))
        self._model['image_dimension_height'] = int(self._config.get('model', 'input_image_dimension_height'))
        self._model['image_dimension_width'] = int(self._config.get('model', 'input_image_dimension_width'))
        self.model = Model(self._model)

        self._redis['host'] = self._config.get('redis', 'host')
        self._redis['port'] = int(self._config.get('redis', 'port'))
        self._redis['db'] = int(self._config.get('redis', 'db'))
        self._redis['queue'] = self._config.get('redis', 'queue')
        self.redis = Redis(self._redis)


config = Config()
