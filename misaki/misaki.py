import misaki.model
from misaki.utils import progressbar


def prepare(input_path: str):
    """
    Prepares dataset in path for training. Expects input_path to be a directory which holds a subdirectory for
    each class. Is a wrapper for the prepare func in misaki.model and also provides console output.
    :param str input_path: path of input data top directory
    """
    def callback_progress_class(nb_class, directory):
        print('[INFO] Preparing class {} in {}'.format(nb_class, directory))

    def callback_progressbar_image(i, total):
        progressbar(i, total, 'Preparing')

    misaki.model.prepare_dataset(input_path, callback_progress_class=callback_progress_class,
                                 callback_progressbar_image=callback_progressbar_image)


def train(test_size=0.2):
    """
    Trains a new model with the prepared dataset. Is a wrapper for the prepare func in misaki.model and
    also provides console output.
    :param float test_size: how much data in % should be reserved for testing
    """
    (history, scores) = misaki.model.train(test_size)

    print('[INFO] Test Loss:', scores[0])
    print('[INFO] Test Accuracy:', scores[1])


def predict(input_path: str):
    """
    Loads image in input_path and uses the latest model to do a prediction. Is a wrapper for the prepare func in
    misaki.model and also provides console output.
    :param str input_path: input image file
    """
    results = misaki.model.predict_from_path(input_path)
    i = 1
    for result in results:
        print('[INFO] Result {}'.format(i))
        i += 1
        for prediction in result:
            print('class={} uid={} value={}'.format(prediction['nb_class'], prediction['uid'], prediction['value']))
