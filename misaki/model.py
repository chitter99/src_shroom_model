import base64
import io
import os
import csv
import numpy
from PIL import Image

from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras.utils import np_utils

from misaki.config import config


# Hotfix for Keras issue with Tensorflow version
# https://github.com/keras-team/keras/issues/13684
import keras.backend.tensorflow_backend as tfback
import tensorflow as tf


def _get_available_gpus():
    if tfback._LOCAL_DEVICES is None:
        devices = tf.config.list_logical_devices()
        tfback._LOCAL_DEVICES = [x.name for x in devices]
    return [x for x in tfback._LOCAL_DEVICES if 'device:gpu' in x.lower()]


tfback._get_available_gpus = _get_available_gpus

# end hotfix


def get_dim():
    """
    Returns the input image dimensions
    :return: tuple
    """
    return config.model.image_dimension_width, config.model.image_dimension_height


def callbacks():
    """
    Returns a callback which does checkpoints for the fit func.
    :return: array
    """
    # This exports the current model after each epoch.
    # In case of a sudden shutdown of the fitting, we would only lose the current epoch.
    checkpoint = ModelCheckpoint(config.path.output_checkpoint_models, monitor='loss',
                                 verbose=0, save_best_only=True, mode='min')
    return [checkpoint]


def get_model(input_shape, classes=config.model.classes):
    """
    Sets up a sequential model for image classification.
    :param tuple input_shape: shape of input
    :param int classes: number of classes
    :return: Sequential
    """
    model = Sequential()
    model.add(Convolution2D(32, kernel_size=(3, 3), data_format='channels_first', activation='relu',
                            input_shape=input_shape))
    model.add(Convolution2D(32, kernel_size=(3, 3), data_format='channels_first', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(learning_rate=0.0001, decay=1e-6), metrics=['accuracy'])
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model


def prepare_input_data(arr, dim=get_dim()):
    """
    Prepares a input data array.
    :param array arr: input data shape of (n_images, data)
    :param tuple dim: target dimension
    :return: array
    """
    # reshape input to match (n_images, channels, x_shape, y_shape)
    arr = arr.reshape(arr.shape[0], 3, dim[0], dim[1])
    arr = arr.astype('float32')
    # scale rgb from 0 to 1 instead of 0 to 255, should speed up the network :)
    arr /= 255
    return arr


def prepare_image_matrix(image_matrix, classes=config.model.classes, dim=get_dim(), test_size=0.2):
    """
    Splits a image_matrix into test and train parts and prepares all data correctly.
    :param tuple image_matrix: image_matrix should match (images, labels)
    :param int classes: number of classes
    :param tuple dim: target dimension (width, height)
    :param float test_size: how much data in % should be reserved for testing
    :return: tuple: (x_train, y_train, x_test, y_test)
    """
    (x, y) = image_matrix
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size)

    x_train = prepare_input_data(x_train, dim)
    x_test = prepare_input_data(x_test, dim)

    y_train = np_utils.to_categorical(y_train, classes)
    y_test = np_utils.to_categorical(y_test, classes)

    return x_train, y_train, x_test, y_test


def prepare_dataset(input_path: str, callback_progress_class=None, callback_progressbar_image=None):
    """
    Prepares dataset in path for training. Expects input_path to be a directory which holds a subdirectory for
    each class.
    :param str input_path: path of input data top directory
    :param callable callback_progress_class: will be called when we move on to the next class
    :param callable callback_progressbar_image: will be called each time a image is processed
    """
    dataset = [sub for sub in os.walk(input_path)]
    dataset.pop(0)  # we don't need the top level directory
    dataset = sorted(dataset, key=lambda sub: sub[0])  # sort by directory name to ensure class order

    # make sure classes found in input data do not exceed the amount defined
    if not len(dataset) == config.model.classes:
        raise Exception('make sure classes found in input data do not exceed the amount defined in the config')

    dim = get_dim()
    images = []
    labels = []

    nb_class = 0  # current class number
    for s in dataset:
        i = 0
        i_max = len(s[2])

        if callback_progress_class is not None:
            callback_progress_class(nb_class, s[0])

        for image_name in s[2]:
            image_path = os.path.join(s[0], image_name)
            image = Image.open(image_path)

            # resize image if not in correct dimension
            if not image.size == dim:
                image = image.resize(dim)

            arr = numpy.array(image).flatten()

            images.append(arr)
            labels.append(nb_class)

            i += 1
            if callback_progressbar_image is not None:
                callback_progressbar_image(i, i_max)
        nb_class += 1

    print('[INFO] Prepared total of {} images!'.format(len(labels)))

    # make sure images are saved in a float32 array
    image_matrix = (numpy.array(images, 'f'), numpy.array(labels))
    save_matrix(image_matrix)


def save_matrix(image_matrix: tuple):
    """
    Saves matrix to file in npz format.
    :param tuple image_matrix: should match (images, labels)
    """
    print('[INFO] Saving matrix...')
    numpy.savez_compressed(config.path.output_matrix, images=image_matrix[0], labels=image_matrix[1])
    print('[INFO] Saved matrix in {}'.format(config.path.output_matrix))


def load_matrix() -> tuple:
    """
    Loads matrix saved in npz format
    :return: tuple: matches format (images, labels)
    """
    print('[INFO] Loading matrix...')

    loaded = numpy.load(config.path.output_matrix, allow_pickle=True)
    image_matrix = (loaded['images'], loaded['labels'])

    print('[INFO] Loaded matrix!')

    return image_matrix


def train(test_size=0.2) -> tuple:
    """
    Trains a new model with the prepared dataset.
    :param float test_size: how much data in % should be reserved for testing
    :return tuple (history, scores)
    """
    print('[INFO] Preparing...')

    (x_train, y_train, x_test, y_test) = prepare_image_matrix(load_matrix(), test_size=test_size)
    model = get_model(x_train.shape[1:])

    print('[INFO] Prepared!')
    print('[INFO] Started fitting...')

    history = model.fit(x_train, y_train, batch_size=config.model.batch_size,
                        epochs=config.model.epochs, callbacks=callbacks(), verbose=1)

    print('[INFO] Fitting done!')
    print('[INFO] Saving model to {}'.format(config.path.output_model))

    model.save(config.path.output_model)

    print('[INFO] Saved model!')
    print('[INFO] Evaluating model...')

    scores = model.evaluate(x_test, y_test, verbose=1)

    print('[INFO] Evaluated!')

    return history, scores


def predict_from_path(input_path: str):
    """
    Loads image from path and uses model to predict class
    :param str input_path: input image path
    :return: array
    """
    return predict(Image.open(input_path))


def predict_from_base64(data):
    """
    Base64 decodes input data and uses model to predict class
    :param array data: input data encoded in base64
    :return: array
    """
    input_data = io.BytesIO(base64.b64decode(data))
    input_data.seek(0)  # make sure cursor is at the start of the data
    return predict(Image.open(input_data))


def predict(image: Image):
    """
    Uses model to classify input image
    :param Image image: input image
    :return: array
    """
    dim = get_dim()

    # Resize image if not in correct dimension
    if not image.size == dim:
        image = image.resize(dim)

    # prepare_input_data expects this shape (n_images, data)
    arr = prepare_input_data(numpy.array([numpy.array(image).flatten()]))

    model = get_model(arr.shape[1:])
    model.load_weights(config.path.output_model)

    result = model.predict(arr)
    return translate_result_classes(result)


def translate_result_classes(results):
    """
    Translates prediction results from Keras with correct id's for each class.
    :param array results: Keras prediction
    :return: array
    """
    lexicon = read_classes_lexicon()
    output_results = []
    for result in results:
        nb_class_result = []
        nb_class = 0
        for prediction in result:
            nb_class_result.append({
                'uid': lexicon[nb_class],
                'nb_class': nb_class,
                'value': format(float(prediction), 'f')
            })
            nb_class += 1
        output_results.append(nb_class_result)
    return output_results


def read_classes_lexicon():
    """
    Reads classes_lexicon csv and returns lexicon to match nb_class with uid.
    :return: array
    """
    lexicon = []
    with open(config.path.classes_lexicon) as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            lexicon.append(row[1])
    return lexicon
