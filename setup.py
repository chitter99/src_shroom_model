from setuptools import setup, find_packages


# read requirements file as the required imports are defined their
with open('./requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name='misaki',
    version='1.0',
    descirption='Mushroom classification model for my cool mushroom app. Called Misaki.',
    author='Aaron Schmid',
    author_email='aaron.schmid@outlook.com',
    url='https://whatmushroom.app/',
    packages=find_packages(include=['misaki', 'misaki.*']),
    install_requires=install_requires,
    scripts=[
        './scripts/misaki-worker',
        './scripts/misaki-cli'
    ]
)
