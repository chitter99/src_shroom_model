FROM python:3.7-buster
LABEL maintainer "Aaron Schmid <aaron.schmid@outlook.com>"

# installing nvidea and cuda drivers
# as of the time of writing this dockerfile, cuda's official dockerimage only supports up to Ubuntu 18.04
# but we are using the python's debian buster image, luckly this still worked :)
# https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/ubuntu18.04/10.2/base/Dockerfile

RUN apt-get update && apt-get install -y --no-install-recommends \
gnupg2 curl ca-certificates && \
    curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
    echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
    apt-get purge --autoremove -y curl && \
rm -rf /var/lib/apt/lists/*

ENV CUDA_VERSION 10.2.89

ENV CUDA_PKG_VERSION 10-2=$CUDA_VERSION-1

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update && apt-get install -y --no-install-recommends \
        cuda-cudart-$CUDA_PKG_VERSION \
cuda-compat-10-2 && \
ln -s cuda-10.2 /usr/local/cuda && \
    rm -rf /var/lib/apt/lists/*

# Required for nvidia-docker v1
RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.2 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=396,driver<397 brand=tesla,driver>=410,driver<411 brand=tesla,driver>=418,driver<419"

# installing misaki
WORKDIR /app/misaki

# install all dependencies before moving code
# to make sure docker layer caching is used
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

# remove the local config and replace it with
# the one that should be used in a docker env
RUN rm ./config.ini &&\
    mv ./config.docker.ini ./config.ini

RUN chmod +x /app/misaki/scripts/*

# install misaki as module
# currently if we install our module via setuptools
# it messes up our config files
# RUN pip3 install .
# so instead we just gonna add the module directory to the path
# python will now find our module when we try to import it
ENV PYTHONPATH ${PYTHONPATH}:/app/misaki/

VOLUME /app/misaki/volume

ENTRYPOINT exec /app/misaki/scripts/misaki-worker
