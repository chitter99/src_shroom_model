# IPA: Misaki

## Usage
There are two runtime exposed by this module. A cli and a worker runtime.
With the cli runtime you can interact like any other console interface.
The worker runtime connects to redis and waits for tasks to be pushed into a que.

### How to use the cli
#### Training model
```bash
misaki-cli prepare ./data
misaki-cli train --test-size 0.2
```

#### Making prediction
```bash
misaki-cli predict ./samples/mushroom_1.jpg --output ./out.json
```

## Installation

### via setup.py
You can install this module via setuptools.

```bash
pip install .
```
or
```bash
python setup.py install
```

### via docker image
There is also a convenient Dockerfile available.

```bash
sudo docker build -t ipa\misaki .
sudo docker run --volume=volume:/app/misaki/volume:rw --net=host --name misaki ipa\misaki
```

